#ifndef _fila_int_h
#define _fila_int_h

void cria_fila_int();
void enfileira_int(int y);
void reiniciar_fila_int();
void imprimir_fila_int();
int sair();
int desenfileira_int();
int fila_int_vazia();
int fila_int_cheia();
int inserir_elementos();
int remover_elementos();

#endif