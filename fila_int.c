#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fila_int.h"
#define N 10

static int fila_int[N];
static int p, u;

void cria_fila_int() {

    p = 0; u = 0;
}

void enfilera_int(int y) {

    fila_int[u++] = y;
}

void imprimir_fila_int(int fila_int[N]) {

    for(int i = 0; i < strlen(fila_int); i++)
        printf("%d ", fila_int[i]);
}

int sair() {
    
    exit(0);
}

int desenfileira_int() {

    return fila_int[p++];
}

int fila_int_vazia() {

    if(p == u)
        cria_fila_int();
    
    return p == u;
}

int fila_int_cheia() {

    return u == N;
}

