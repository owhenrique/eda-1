#include <stdio.h>
#include <stdlib.h>
#include "fila_int.h"

int main() {

    int opcao_menu;

    printf("Escolha uma das opções:\n");
    printf("1. Inserirnelementos na fila.\n");
    printf("2. Removernelementos na fila.\n");
    printf("3. Reiniciar a fila.\n");
    printf("4. Imprimir a fila.\n");
    printf("5. Sair.\n");

    while(opcao_menu != 5) {
        
        scanf("%d", &opcao_menu);

        system("clear");

        if(opcao_menu == 4){

            imprimir_fila_int();
        }

        else if(opcao_menu == 5) {

            printf("Good Bye!\n");
            sair();
        }

        else {
            printf(":S\n");
        }
    }

    return 0;
}